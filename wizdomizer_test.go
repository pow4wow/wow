package wow

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	w Wisdomizer
)

func TestMain(m *testing.M) {
	w = NewWisdomizer()

	os.Exit(m.Run())
}

func TestWisdom_GetWord(t *testing.T) {
	for i := 1 << 10; i != 0; i-- {
		word := w.GetWord()

		assert.NotEmpty(t, word)
	}
}
