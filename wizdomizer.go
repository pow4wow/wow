package wow

import (
	"bufio"
	"bytes"
	_ "embed"
	"math/rand"
)

//go:embed dicts/quotes
var wisdomData []byte

type Wisdomizer interface {
	GetWord() string
}

type wisdom struct {
	words []string
}

func (w wisdom) GetWord() string {
	i := rand.Intn(len(w.words))
	return w.words[i]
}

func NewWisdomizer() Wisdomizer {
	w := wisdom{
		words: make([]string, 0),
	}

	buff := bytes.NewBuffer(wisdomData)
	scanner := bufio.NewScanner(buff)

	for scanner.Scan() {
		w.words = append(w.words, scanner.Text())
	}

	return w
}
